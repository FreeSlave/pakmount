CC=cc
LINK=$(CC)
BUILD_DIR=build
BIN_DIR=bin
SRC_DIR=source
OBJ_DIR=$(BUILD_DIR)

OBJS = \
	$(OBJ_DIR)/main.o \
	$(OBJ_DIR)/paktree.o \
	$(OBJ_DIR)/pak.o \
	$(OBJ_DIR)/wad.o \
	$(OBJ_DIR)/vpk.o

PROGRAM=pakmount
TARGET=$(BIN_DIR)/$(PROGRAM)

all: make_obj_dir make_bin_dir $(TARGET)

make_obj_dir: 
	-mkdir -p $(OBJ_DIR)

make_bin_dir: 
	-mkdir -p $(BIN_DIR)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) `pkg-config fuse --cflags` $(INCLUDE) $(CFLAGS) $(USER_FLAGS) -o $@ -c $<

$(TARGET): $(OBJS)
	$(LINK) $(CFLAGS) $(USER_FLAGS) $(OBJS) `pkg-config fuse --libs` -o $@

clean:
	rm -f $(OBJ_DIR)/*.o

distclean: clean
	rm $(TARGET)

