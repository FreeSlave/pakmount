/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "paktree.h"

static void separatePathAndNameN(const char* path, size_t size, size_t* pathSize, const char** pfileName) {
    size_t len = size;
    for (len--;len && path[len] != '/'; --len);
    if (len == 0) {
        *pathSize = 1;
    } else {
        *pathSize = len;
    }
    *pfileName = path+len+1;
}

int isFileNode(TreeNode* node) {
    return node->type == S_IFREG;
}
int isDirectoryNode(TreeNode* node) {
    return node->type == S_IFDIR;
}

static void initNode(TreeNode* node)
{
    if (node) {
        node->name = NULL;
        node->type = S_IFDIR;
        node->data = NULL;
        node->size = 0;
        node->children = NULL;
        node->childCount = 0;
        node->parent = NULL;
    }
}

void setName(TreeNode* node, const char* name)
{
    size_t len = strlen(name);
    if (node->name) {
        if (len > strlen(node->name)) {
            node->name = (char*)realloc(node->name, len+1);
        }
    } else {
        node->name = (char*)malloc(len+1);
    }
    strcpy(node->name, name);
}

TreeNode* createNode(const char* name) {
    if (name) {
        TreeNode* node = malloc(sizeof(TreeNode));
        initNode(node);
        setName(node, name);
        return node;
    }
    return NULL;
}

void addChildNode(TreeNode* parent, TreeNode* child) {
    if (parent && child) {
        if (parent->children == NULL) {
            parent->children = (TreeNode**)malloc(sizeof(TreeNode*));
            parent->childCount = 1;
        } else {
            TreeNode** realloced = (TreeNode**)realloc(parent->children, (parent->childCount+1) * sizeof(TreeNode*));
            parent->children = realloced;
            parent->childCount++;
        }
        parent->children[parent->childCount-1] = child;
        child->parent = parent;
    }
}

TreeNode* findChildNodeN(TreeNode* node, const char* name, size_t size) {
    if (node && name) {
        int i;
        for (i = 0; i<node->childCount; ++i) {
            if (strncmp(node->children[i]->name, name, size) == 0) {
                return node->children[i];
            }
        }
    }
    return NULL;
}

TreeNode* findChildNode(TreeNode* node, const char* name) {
    if (node && name) {
        return findChildNodeN(node, name, strlen(name));
    }
    return NULL;
}

static TreeNode* findNodeByPathN(TreeNode* root, const char* path, size_t size)
{
    if (root && path) {
        if (path[0] == '/') {
            if (size == 1) {
                return root;
            } else {
                TreeNode* parent = root;
                size_t overallSize = 1;
                while(overallSize < size) {
                    size_t len = 0;
                    while(overallSize + len != size && path[overallSize+len] != '/') {
                        len++;
                    }
                    TreeNode* node = findChildNodeN(parent, path + overallSize, len);
                    overallSize += len;
                    if (node) {
                        if (overallSize == size) {
                            return node;
                        } else if (isDirectoryNode(node)) {
                            parent = node;
                            overallSize++;
                        } else {
                            return NULL;
                        }
                    } else {
                        return NULL;
                    }
                }
            }
        }
    }
    return NULL;
}

TreeNode* findNodeByPath(TreeNode* root, const char* name)
{
    return findNodeByPathN(root, name, strlen(name));
}

void freeTree(TreeNode* root) {
    if (root) {
        size_t i = 0;
        for (; i<root->childCount; ++i) {
            freeTree(root->children[i]);
        }
        free(root->name);
        free(root->data);
        free(root->children);
        free(root);
    }
}

static int makeOrphan(TreeNode* node)
{
    TreeNode* parent = node->parent;
    if (!parent) {
        return EINVAL;
    }
    if (!node) {
        return EINVAL;
    }
    
    size_t i;
    for (i=0; i<parent->childCount; ++i) {
        if (parent->children[i] == node) {
            parent->children[i] = NULL;
            for (; i<parent->childCount-1; ++i) {
                parent->children[i] = parent->children[i+1];
            }
            parent->childCount--;
            return 0;
        }
    }
    return ENOENT;
}

static int removeNode(TreeNode* node)
{
    if (!node) {
        return ENOENT;
    }
    int ret = makeOrphan(node);
    if (ret == 0) {
        freeTree(node);
    }
    return ret;
}

int removeDirNode(TreeNode* root, const char* path)
{
    TreeNode* node = findNodeByPath(root, path);
    if (node) {
        if (isDirectoryNode(node)) {
            return removeNode(node);
        } else {
            return ENOTDIR;
        }
    } else {
        return ENOENT;
    }
}

int removeFileNode(TreeNode* root, const char* path)
{
    TreeNode* node = findNodeByPath(root, path);
    if (node) {
        if (isDirectoryNode(node)) {
            return EISDIR;
        } else {
            return removeNode(node);
        }
    } else {
        return ENOENT;
    }
}

static int insertNode(TreeNode* root, const char* path, int type)
{
    size_t pathSize;
    const char* fileName;
    separatePathAndNameN(path, strlen(path), &pathSize, &fileName);
    
    TreeNode* parent = findNodeByPathN(root, path, pathSize);
    if (parent) {
        TreeNode* node = findChildNode(parent, fileName);
        if (node) {
            return EEXIST;
        } else {
            node = createNode(fileName);
            node->type = type;
            addChildNode(parent, node);
            return 0;
        }
    } else {
        return ENOENT;
    }
}

int insertDirNode(TreeNode* root, const char* path)
{
    return insertNode(root, path, S_IFDIR);
}

int insertFileNode(TreeNode* root, const char* path)
{
    return insertNode(root, path, S_IFREG);
}

int renameFileNode(TreeNode* root, const char* from, const char* to)
{
    size_t fromPathEnd;
    const char* fromFileName;
    separatePathAndNameN(from, strlen(from), &fromPathEnd, &fromFileName);
    
    TreeNode* fromParent = findNodeByPathN(root, from, fromPathEnd);
    if (!fromParent) {
        return ENOENT;
    }
    
    TreeNode* fromNode = findChildNode(fromParent, fromFileName);
    if (!fromNode) {
        return ENOENT;
    }
    
    size_t toPathEnd;
    const char* toFileName;
    separatePathAndNameN(to, strlen(to), &toPathEnd, &toFileName);
    
    TreeNode* toParent = findNodeByPathN(root, to, toPathEnd);
    if (!toParent) {
        return ENOENT;
    }
    
    TreeNode* toNode = findChildNode(fromParent, fromFileName);
    if (toNode) {
        if (isDirectoryNode(toNode)) {
            if (isFileNode(fromNode)) {
                return EISDIR;
            }
            if (toNode->childCount) {
                return ENOTEMPTY;
            } else {
                removeDirNode(root, to);
            }
        } else {
            if (isDirectoryNode(fromNode)) {
                return ENOTDIR;
            }
            removeFileNode(root, to);
        }
    }
    
    makeOrphan(fromNode);

    setName(fromNode, toFileName);
    addChildNode(toParent, fromNode);

    return 0;
}

int truncateFileNode(TreeNode* root, const char* path, off_t size)
{
    TreeNode* node = findNodeByPath(root, path);
    if (!node) {
        return ENOENT;
    }
    if (isDirectoryNode(node)) {
        return EISDIR;
    }
    
    if (size == 0) {
        free(node->data);
        node->data = NULL;
        node->size = 0;
    } else if (size > node->size) {
        void* realloced = realloc(node->data, size);
        if (!realloced) {
            return ENOMEM;
        } else {
            node->data = realloced;
            memset(node->data + node->size, 0, size - node->size);
            node->size = size;
        }
    } else if (size < node->size) {
        node->size = size;
    }
    
    return 0;
}

int writeToFileNode(TreeNode* root, const char *path, const char *data, size_t size, off_t offset)
{
    TreeNode* node = findNodeByPath(root, path);
    if (!node) {
        return -ENOENT;
    }
    if (isDirectoryNode(node)) {
        return -EISDIR;
    }
    
    size_t len = offset + size;
    
    if (len) {
        if (!node->data) {
            node->data = malloc(len);
            if (!node->data) {
                return -ENOMEM;
            }
        } else {
            void* realloced = realloc(node->data, len);
            if (!realloced) {
                return -ENOMEM;
            }
            node->data = realloced;
        }
        memcpy(node->data + offset, data, size);
        node->size = len;
    }
    
    return size;
}

