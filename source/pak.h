/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PAKFILE_H
#define PAKFILE_H

#include "pakformat.h"

#define MAX_NAME_LENGTH 56

#pragma pack(1)
typedef struct
{
    char name[MAX_NAME_LENGTH];
    int filepos;
    int filelen;
} lump_t;

typedef struct
{
    int dirofs;
    int dirlen;
} dlump_t;
#pragma pack()

enum {
    IDPAKHEADER = (('K'<<24)+('C'<<16)+('A'<<8)+'P')
};

void initPakFormat(struct pakformat *handler);

#endif // PAKFILE_H
