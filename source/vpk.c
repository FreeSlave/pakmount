/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>

#include "vpk.h"
#define HL_VPK_SIGNATURE 0x55aa1234

static int canRead(FILE* pakFile)
{
    VPKHeader header;
    if (fread(&header, sizeof(VPKHeader), 1, pakFile) != 1) {
        return 0;
    }
    return header.Signature == HL_VPK_SIGNATURE;
}

static TreeNode* build(void* pakBuf, size_t pakBufSize, const struct stat* pakStat)
{
    const char* reasonInvalid = "invalid or corrupted file";
    
    if (pakBufSize < sizeof(VPKHeader)) {
        fprintf(stderr, "Could not read VPK header: %s\n", reasonInvalid);
        return NULL;
    }
    VPKHeader* header = (VPKHeader*)pakBuf;
    if (header->Version > 2) {
        fprintf(stderr, "Unsupported VPK archive version\n");
        return NULL;
    }
    VPKExtendedHeader* eHeader = NULL;
    if (header->Version == 2) {
        if (pakBufSize < sizeof(VPKHeader) + sizeof(VPKExtendedHeader)) {
            fprintf(stderr, "Could not read VPK header version 2: %s\n", reasonInvalid);
            return NULL;
        }
    }
    
    void* dataStart = pakBuf + sizeof(VPKHeader) + (eHeader ? sizeof(VPKExtendedHeader) : 0);
    void* dataEnd = pakBuf + pakBufSize;
    if (dataEnd - dataStart < header->TreeLength) {
        fprintf(stderr, "Could not read directory tree: %s\n", reasonInvalid);
        return NULL;
    }
    
    while(1) {
        const char* extension = (const char*)dataStart;
        dataStart += strlen(extension) + 1;
        if (*extension == '\0') {
            break;
        }
        while(1) {
            const char* path = (const char*)dataStart;
            dataStart += strlen(path) + 1;
            if (*path == '\0') {
                break;
            }
            while(1) {
                const char* fileName = (const char*)dataStart;
                dataStart += strlen(fileName) + 1;
                if (*fileName == '\0') {
                    break;
                }
                if (dataEnd - dataStart < sizeof(VPKDirectoryEntry)) {
                    fprintf(stderr, "Could not read directory entry: %s\n", reasonInvalid);
                    return NULL;
                }
                printf("extension: %s. path: %s. fileName: %s\n", extension, path, fileName);
                VPKDirectoryEntry* entry = dataStart;
                dataStart += sizeof(VPKDirectoryEntry);
            }
        }
    }
    
    return NULL;
}

void initVpkFormat(struct pakformat *handler)
{
    strcpy(handler->name, "vpk");
    handler->canRead = canRead;
    handler->build = build;
    handler->save = NULL;
    handler->maxFileNameLength = 512;
    handler->supportsSubdirs = 1;
}
