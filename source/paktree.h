/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef PAKTREE_H
#define PAKTREE_H

#include <stddef.h>
#include <time.h>

typedef struct TreeNode_s
{
    char* name;
    
    int type;
    
    //file data
    void* data;
    size_t size;
    
    //items in directory
    struct TreeNode_s** children;
    size_t childCount;
    
    //parent directory
    struct TreeNode_s* parent;
    
    //times
    time_t atime;
    time_t mtime;
    
} TreeNode;

TreeNode* createNode(const char* name);
void addChildNode(TreeNode* parent, TreeNode* child);
TreeNode* findChildNodeN(TreeNode* node, const char* name, size_t size);
TreeNode* findChildNode(TreeNode* node, const char* name);
TreeNode* findNodeByPath(TreeNode* root, const char* path);
void freeTree(TreeNode* root);
int removeDirNode(TreeNode* root, const char* path);
int removeFileNode(TreeNode* root, const char* path);
int insertDirNode(TreeNode* root, const char* path);
int insertFileNode(TreeNode* root, const char* path);
int renameFileNode(TreeNode* root, const char* from, const char* to);
int truncateFileNode(TreeNode* root, const char* path, off_t size);
int writeToFileNode(TreeNode* root, const char *path, const char *data, size_t size, off_t offset);

int isFileNode(TreeNode* node);
int isDirectoryNode(TreeNode* node);

#endif
