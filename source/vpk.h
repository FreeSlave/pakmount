/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef VPKFILE
#define VPKFILE

#include "pakformat.h"

// format description: https://developer.valvesoftware.com/wiki/VPK_File_Format

#pragma pack(1)

typedef struct
{
    unsigned int Signature; //0x55aa1234.
    unsigned int Version;
    unsigned int TreeLength;
} VPKHeader;

// Added in version 2.
typedef struct 
{
    int Unknown1;
    unsigned int FooterLength;
    int Unknown3; 
    int Unknown4;
} VPKExtendedHeader;

typedef struct 
{
    unsigned int CRC; // A 32bit CRC of the file's data.
    unsigned short PreloadBytes; // The number of bytes contained in the index file.
 
    // A zero based index of the archive this file's data is contained in.
    // If 0x7fff, the data follows the directory.
    unsigned short ArchiveIndex;
 
    // If ArchiveIndex is 0x7fff, the offset of the file data relative to the end of the directory (see the header for more details).
    // Otherwise, the offset of the data from the start of the specified archive.
    unsigned int EntryOffset;
 
    // If zero, the entire file is stored in the preload data.
    // Otherwise, the number of bytes stored starting at EntryOffset.
    unsigned int EntryLength;
 
    const unsigned short Terminator; //0xffff
} VPKDirectoryEntry;

#pragma pack()

void initVpkFormat(struct pakformat *handler);

#endif
