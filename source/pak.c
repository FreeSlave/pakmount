/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>

#include "pak.h"

static int canRead(FILE* pakFile)
{
    int id;
    if (fread(&id, sizeof(int), 1, pakFile) != 1) {
        return 0;
    }
    return id == IDPAKHEADER;
}

static void countLeaves(TreeNode* node, int *count)
{
    if (node) {
        if (isDirectoryNode(node)) {
            size_t i;
            for (i=0; i<node->childCount; ++i) {
                countLeaves(node->children[i], count);
            }
        } else {
            if (node->size) {
                *count += 1;
            }
        }
    }
}

static void savePakFileHelper(TreeNode* parent, char* str, size_t top, lump_t* lumps, int* lumpnum, FILE* file)
{
    size_t i;
    for (i=0; i<parent->childCount; ++i) {
        TreeNode* node = parent->children[i];
        if (node) {
            if (isDirectoryNode(node)) {
                sprintf(str + top, "%s/", node->name);
                savePakFileHelper(node, str, top + strlen(node->name) + 1, lumps, lumpnum, file);
            } else {
                if (node->size) {
                    strcpy(str + top, node->name);
                    strcpy(lumps[*lumpnum].name, str);
                    lumps[*lumpnum].filepos = (int)ftell(file);
                    lumps[*lumpnum].filelen = (int)node->size;
                    fwrite(node->data, 1, node->size, file);
                    *lumpnum += 1;
                }
            }
        }
    }
}

static int save(TreeNode* treeRoot, const char* where)
{
    FILE* file = fopen(where, "w");
    if (!file) {
        return -1;
    }
    int header = IDPAKHEADER;
    fwrite(&header, sizeof(header), 1, file);
    
    int count = 0;
    countLeaves(treeRoot, &count);
    
    int dirofs = 12, dirlen = count*sizeof(lump_t);
    fwrite(&dirofs, sizeof(dirofs), 1, file);
    fwrite(&dirlen, sizeof(dirlen), 1, file);
    
    lump_t* lumps = malloc(count * sizeof(lump_t));
    
    fwrite(lumps, sizeof(lump_t), count, file); //fill space with invalid data
    
    int lumpnum = 0;
    char buf[MAX_NAME_LENGTH+1] = {'\0'};
    savePakFileHelper(treeRoot, buf, 0, lumps, &lumpnum, file);
    
    fseek(file, dirofs, SEEK_SET);
    fwrite(lumps, sizeof(lump_t), count, file); //rewrite invalid data
    
    free(lumps);
    
    if (ferror(file)) {
        fclose(file);
        return -1;
    }
    
    fclose(file);
    return 0;
}

static TreeNode* build(void* pakBuf, size_t pakBufSize, const struct stat* pakStat)
{
    if (pakBufSize < sizeof(int) + sizeof(dlump_t)) {
        fprintf(stderr, "Invalid or corrupted file\n");
        return NULL;
    }
    
    void* cur = pakBuf + sizeof(int);
    dlump_t* dlump = (dlump_t*) cur;
    
    if (dlump->dirofs > pakBufSize || dlump->dirlen > pakBufSize || 
        dlump->dirofs + dlump->dirlen > pakBufSize) {
        fprintf(stderr, "Invalid dir data\n");
        return NULL;
    }
    
    cur = pakBuf + dlump->dirofs;
    size_t lumpCount = dlump->dirlen / sizeof(lump_t);
    
    TreeNode* treeRoot = createNode("");
    treeRoot->atime = pakStat->st_atime;
    treeRoot->mtime = pakStat->st_mtime;
    
    int i;
    for (i=0; i<lumpCount; ++i)
    {
        lump_t* lump = (lump_t*) cur;
        if (lump->filepos > pakBufSize || lump->filelen > pakBufSize || 
            lump->filepos + lump->filelen > pakBufSize) {
            fprintf(stderr, "Invalid lump\n");
            return NULL;
        }
        
        TreeNode* parent = treeRoot;
        TreeNode* child = NULL;
        int start = 0, j = 0;
        for(; j < MAX_NAME_LENGTH && lump->name[j] != '\0'; ++j)
        {
            if (lump->name[j] == '/') {
                size_t size = j - start;
                child = findChildNodeN(parent, lump->name + start, size);
                if (child == NULL) {
                    char s[MAX_NAME_LENGTH+1] = {'\0'};
                    strncat(s, lump->name + start, size);
                    child = createNode(s);
                    child->atime = pakStat->st_atime;
                    child->mtime = pakStat->st_mtime;
                    addChildNode(parent, child);
                }
                parent = child;
                start = j+1;
            }
        }
        child = createNode(lump->name + start);
        child->atime = pakStat->st_atime;
        child->mtime = pakStat->st_mtime;
        void* data = malloc(lump->filelen);
        memcpy(data, pakBuf+lump->filepos, lump->filelen);
        child->data = data;
        child->size = lump->filelen;
        child->type = S_IFREG;
        
        addChildNode(parent, child);
        
        cur += sizeof(lump_t);
    }
    return treeRoot;
}

void initPakFormat(struct pakformat *handler)
{
    strcpy(handler->name, "pak");
    handler->canRead = canRead;
    handler->build = build;
    handler->save = save;
    handler->maxFileNameLength = MAX_NAME_LENGTH;
    handler->supportsSubdirs = 1;
}

