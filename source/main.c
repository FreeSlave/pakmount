/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

//C library
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <time.h>

//System headers
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <unistd.h>

#include <pthread.h>

//Fuse headers
#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <fuse_opt.h>

#include "paktree.h"
#include "pak.h"
#include "wad.h"
#include "vpk.h"

#define PAKMOUNT_VERSION "0.1.1"

enum
{
    KEY_VERSION,
    KEY_HELP,
};

struct options 
{
    int writable;
    int nobackup;
    int nosave;
};

static char* pakFileName = NULL;
static char* mountPoint = NULL;
static struct options options;
static TreeNode* treeRoot = NULL;
static char backupPak[PATH_MAX] = {'\0'};
static int modified = 0;
pthread_mutex_t lock;

static struct pakformat handlers[3];
static struct pakformat* formatHandler = NULL;

static inline int isWritable()
{
    return options.writable;
}

static inline int needBackup()
{
    return isWritable() && !options.nobackup && !options.nosave;
}

static int checkFileName(const char* path)
{
    if (strlen(path) > formatHandler->maxFileNameLength) {
        return ENAMETOOLONG;
    }
    return 0;
}

static void setTimes(TreeNode* node, time_t atime, time_t mtime)
{
    node->atime = atime;
    node->mtime = mtime;
}

static struct fuse_opt pak_opts[] =
{
    { "writable", offsetof(struct options, writable), 1 }, 
    { "nobackup", offsetof(struct options, nobackup), 1 }, 
    { "nosave", offsetof(struct options, nosave), 1 }, 

    FUSE_OPT_KEY("-V",             KEY_VERSION),
    FUSE_OPT_KEY("--version",      KEY_VERSION),
    FUSE_OPT_KEY("-h",             KEY_HELP),
    FUSE_OPT_KEY("--help",         KEY_HELP),
    FUSE_OPT_END
};

static void printUsage( const char *progname )
{
    fprintf(stderr,
            "usage: %s archivepath mountpoint [options]\n"
            "\n"
            "general options:\n"
            "    -o opt,[opt...]        mount options\n"
            "    -h   --help            print help\n"
            "    -V   --version         print version\n"
            "\n"
            "pakmount options:\n"
            "    -o writable            enable write support\n"
            "    -o nobackup            don't create backup\n"
            "    -o nosave              do not save changes upon unmount.\n"
            , progname);
}

static int pak_getattr(const char* path, struct stat *statbuf)
{
    int ret = checkFileName(path);
    if (ret != 0) {
        return -ret;
    }
    
    memset(statbuf, 0, sizeof(struct stat));
    
    pthread_mutex_lock(&lock);
    TreeNode* node = findNodeByPath(treeRoot, path);
    pthread_mutex_unlock(&lock);
    statbuf->st_uid = getuid();
    statbuf->st_gid = getgid();
    
    if (node) {
        statbuf->st_atime = node->atime;
        statbuf->st_mtime = node->mtime;
        
        mode_t perms;
        pthread_mutex_lock(&lock);
        if (isDirectoryNode(node)) {
            if (isWritable()) {
                perms = 0755;
            } else {
                perms = 0555;
            }
            statbuf->st_mode = S_IFDIR | perms;
            statbuf->st_nlink = node->childCount+2;
            statbuf->st_size = 4096;
        } else if (isFileNode(node)) {
            if (isWritable()) {
                perms = 0644;
            } else {
                perms = 0444;
            }
            statbuf->st_mode = S_IFREG | perms;
            statbuf->st_nlink = 1;
            statbuf->st_size = node->size;
        }
        pthread_mutex_unlock(&lock);
        statbuf->st_blocks  = (statbuf->st_size + 511) / 512;
        statbuf->st_blksize = 4096;
    } else {
        ret = -ENOENT;
    }
    
    return ret;
}

static int pak_readdir(const char* path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    (void) offset;
    (void) fi;
    
    int ret = 0;
    pthread_mutex_lock(&lock);
    TreeNode* node = findNodeByPath(treeRoot, path);
    if (node) {
        if (!isDirectoryNode(node)) {
            ret = -ENOTDIR;
        } else {
            filler(buf, ".", NULL, 0);
            filler(buf, "..", NULL, 0);
            
            size_t i = 0;
            for (; i<node->childCount; ++i) {
                filler(buf, node->children[i]->name, NULL, 0);
            }
        }
    } else {
        ret = -ENOENT;
    }
    pthread_mutex_unlock(&lock);
    return ret;
}

static int pak_open(const char *path, struct fuse_file_info *fi)
{
    pthread_mutex_lock(&lock);
    TreeNode* node = findNodeByPath(treeRoot, path);
    pthread_mutex_unlock(&lock);
    
    if (!node) {
        return -ENOENT;
    }
    if (fi->flags & O_WRONLY || fi->flags & O_RDWR) {
        if (!isWritable()) {
            return -EROFS;
        }
    }
    return 0;
}

static int pak_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    (void) fi;
    int ret = 0;
    pthread_mutex_lock(&lock);
    TreeNode* node = findNodeByPath(treeRoot, path);
    if (!node) {
        ret = -ENOENT;
    } else {
        if (offset < node->size) 
        {
            if (offset + size > node->size)
                size = node->size - offset;
            memcpy(buf, node->data + offset, size);
            ret = size;
        }
    }
    
    pthread_mutex_unlock(&lock);
    return ret;
}

static int pak_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    (void)fi;
    if (!isWritable()) {
        return -EROFS;
    }
    int ret = checkFileName(path);
    if (ret != 0) {
        return -ret;
    }
    pthread_mutex_lock(&lock);
    ret = -insertFileNode(treeRoot, path);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}

static int pak_mknod(const char *path, mode_t mode, dev_t dev)
{
    (void)mode;
    (void)dev;
    if (!isWritable()) {
        return -EROFS;
    }
    int ret = checkFileName(path);
    if (ret != 0) {
        return -ret;
    }
    pthread_mutex_lock(&lock);
    ret = -insertFileNode(treeRoot, path);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}

static int pak_write(const char *path, const char *data, size_t size, off_t offset, struct fuse_file_info *fi)
{
    (void)fi;
    if (!isWritable()) {
        return -EROFS;
    }
    int ret;
    pthread_mutex_lock(&lock);
    ret = writeToFileNode(treeRoot, path, data, size, offset);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}

static int pak_truncate(const char* path, off_t size)
{
    if (!isWritable()) {
        return -EROFS;
    }
    int ret;
    pthread_mutex_lock(&lock);
    ret = -truncateFileNode(treeRoot, path, size);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}

static int pak_mkdir(const char *path, mode_t mode)
{
    if (!formatHandler->supportsSubdirs) {
        return -ENOTSUP;
    }
    
    int ret = checkFileName(path);
    if (ret != 0) {
        return -ret;
    }
    if (!isWritable()) {
        return -EROFS;
    }
    pthread_mutex_lock(&lock);
    ret = -insertDirNode(treeRoot, path);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}

static int pak_unlink(const char *path)
{
    if (!isWritable()) {
        return -EROFS;
    }
    int ret;
    pthread_mutex_lock(&lock);
    ret = -removeFileNode(treeRoot, path);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}

static int pak_rmdir(const char *path)
{
    if (!isWritable()) {
        return -EROFS;
    }
    int ret;
    pthread_mutex_lock(&lock);
    ret = -removeDirNode(treeRoot, path);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}


static int pak_utimens( const char *path, const struct timespec timespecs[2] )
{
    if (!isWritable()) {
        return -EROFS;
    }
    int ret = 0;
    pthread_mutex_lock(&lock);
    TreeNode* node = findNodeByPath(treeRoot, path);
    if (node) {
        setTimes(node, timespecs[0].tv_sec, timespecs[1].tv_sec);
        modified = 1;
    } else {
        ret = -ENOENT;
    }
    pthread_mutex_unlock(&lock);
    return ret;
}

static int pak_rename( const char *from, const char *to )
{
    int ret;
    pthread_mutex_lock(&lock);
    ret = -renameFileNode(treeRoot, from, to);
    pthread_mutex_unlock(&lock);
    modified = 1;
    return ret;
}

static int pak_statfs( const char *path, struct statvfs *stbuf )
{
    ( void )path;
    stbuf->f_namemax = formatHandler->maxFileNameLength;
    stbuf->f_bsize = 4096;
    stbuf->f_frsize = stbuf->f_bsize;
    stbuf->f_blocks = stbuf->f_bfree =  stbuf->f_bavail =
        1000ULL * 1024 * 1024 * 1024 / stbuf->f_frsize;
    stbuf->f_files = stbuf->f_ffree = 1000000000;
    return 0;
}

static int pak_chown( const char *path, uid_t uid, gid_t gid )
{
    int ret = 0;
    pthread_mutex_lock(&lock);
    TreeNode* node = findNodeByPath(treeRoot, path);
    pthread_mutex_unlock(&lock);
    if (!node) {
        ret = -ENOENT;
    }
    return ret;
}

static int pak_chmod( const char *path, mode_t mode )
{
    int ret = 0;
    pthread_mutex_lock(&lock);
    TreeNode* node = findNodeByPath(treeRoot, path);
    pthread_mutex_unlock(&lock);
    if (!node) {
        ret = -ENOENT;
    }
    return ret;
}

static struct fuse_operations pak_oper = {
    .getattr        = pak_getattr,
    .readdir        = pak_readdir,
    .open           = pak_open,
    .read           = pak_read,
    .rmdir          = pak_rmdir,
    .unlink         = pak_unlink,
    .mknod          = pak_mknod,
    .create         = pak_create,
    .write          = pak_write,
    .utimens        = pak_utimens,
    .truncate       = pak_truncate,
    .rename         = pak_rename,
    .mkdir          = pak_mkdir,
    .statfs         = pak_statfs,
    .chown          = pak_chown,
    .chmod          = pak_chmod
};

static int pak_opt_proc(void *data, const char *arg, int key, struct fuse_args *outargs)
{
    (void)data;
    switch(key)
    {
    case FUSE_OPT_KEY_OPT:
        return 1;

    case FUSE_OPT_KEY_NONOPT:
        if( !pakFileName ) {
            pakFileName = strdup(arg);
            return 0;
        } else if( !mountPoint ) {
            mountPoint = strdup(arg);
        }
        return 1;

    case KEY_HELP:
        printUsage(outargs->argv[0]);
        fuse_opt_add_arg(outargs, "-ho");
        exit(fuse_main(outargs->argc, outargs->argv, &pak_oper, NULL));

    case KEY_VERSION:
        fprintf(stderr, "pakmount version %s\n", PAKMOUNT_VERSION);
        fuse_opt_add_arg(outargs, "--version");
        exit(fuse_main(outargs->argc, outargs->argv, &pak_oper, NULL));

    default:
        fprintf(stderr, "internal error\n");
        abort();
    }
}

TreeNode* buildTree()
{
    FILE* pakFile = fopen(pakFileName, "rb");
    if (pakFile)
    {
        size_t j;
        for (j=0; !formatHandler && j<sizeof(handlers)/sizeof(struct pakformat); ++j) {
            if (handlers[j].canRead && handlers[j].canRead(pakFile)) {
                formatHandler = &handlers[j];
            }
            if (fseek(pakFile, 0, SEEK_SET) !=0) {
                fprintf(stderr, "Could not fseek to the start of file\n");
                return NULL;
            }
        }
        
        if (!formatHandler) {
            fprintf(stderr, "Unknown (unsupported) format or corrupted file\n");
            return NULL;
        }
        
        size_t pakBufSize;
        
        struct stat pakStat;
        int result = stat(pakFileName, &pakStat);
        if (result == 0) {
            pakBufSize = pakStat.st_size;
        } else {
            perror("Error when stating pakfile");
            return NULL;
        }
        
        void* pakBuf = malloc(pakBufSize);
        if (!pakBuf) {
            perror("Could not allocate buffer");
            return NULL;
        }
        
        size_t readBytes = fread(pakBuf, 1, pakBufSize, pakFile);
        fclose(pakFile);
        
        TreeNode* root = NULL;
        if (readBytes != pakBufSize) {
            fprintf(stderr, "Could not read file into memory\n");
        } else {
            root = formatHandler->build(pakBuf, pakBufSize, &pakStat);
            if (root) {
                if (needBackup()) {
                    sprintf(backupPak, "%s.orig", pakFileName);
                    FILE* backup = fopen(backupPak, "w");
                    if (!backup || fwrite(pakBuf, 1, pakBufSize, backup) != pakBufSize) {
                        perror("Could not create backup");
                        freeTree(root);
                        root = NULL;
                    }
                    if (backup) fclose(backup);
                }
            }
        }
        
        free(pakBuf);
        return root;
    }
    else
    {
        perror("Error opening pak file");
        return NULL;
    }
}

int main(int argc, char** argv)
{   
    int ret = 0;
    struct stat mountStat;
    struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
    if(fuse_opt_parse(&args, &options, pak_opts, pak_opt_proc) == -1)
    {
        ret = EXIT_FAILURE;
    }
    else
    {
        if (!mountPoint)
        {
            fprintf(stderr, "Missing mount point\n");
            ret = EXIT_FAILURE;
        }
        else if (!pakFileName)
        {
            fprintf(stderr, "Missing pak file\n");
            ret = EXIT_FAILURE;
        }
        else
        {
            if (stat(mountPoint, &mountStat) != 0)
            {
                perror("Error when stat'ing mountpoint");
                ret = EXIT_FAILURE;
            }
            else if (!S_ISDIR( mountStat.st_mode ))
            {
                fprintf(stderr, "Mountpoint is not an directory\n");
                ret = EXIT_FAILURE;
            }
            else
            {
                initPakFormat(&handlers[0]);
                initWadFormat(&handlers[1]);
                initVpkFormat(&handlers[2]);
                
                treeRoot = buildTree();
                if (treeRoot)
                {
                    if (isWritable() && !options.nosave && formatHandler->save == NULL) {
                        fprintf(stderr, 
                                "pakmount does not support writing of %s archives for now\n", 
                                formatHandler->name);
                        ret = EXIT_FAILURE;
                    } else {
                        int lockresult = pthread_mutex_init(&lock, NULL);
                        if (lockresult != 0) {
                            fprintf(stderr, "Failed to initialize mutex: %s\n", strerror(lockresult));
                            ret = EXIT_FAILURE;
                        } else {
                            ret = fuse_main(args.argc, args.argv, &pak_oper, NULL);
                            pthread_mutex_destroy(&lock);
                            if (ret == 0) {
                                if (modified && !options.nosave) {
                                    if (formatHandler->save(treeRoot, pakFileName) == 0 && *backupPak) {                                        unlink(backupPak);
                                    }
                                } else if (*backupPak) {
                                    unlink(backupPak);
                                }
                            }
                        }
                    }
                }
            }
        }     
    }
    
    fuse_opt_free_args(&args);
    free(pakFileName);
    free(mountPoint);
    freeTree(treeRoot);
    return ret;
}
