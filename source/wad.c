/*
 *  Copyright (C) 2015 Roman Chistokhodov (freeslave93@gmail.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>

#include "wad.h"

static int canRead(FILE* pakFile)
{
    char magic[4];
    if (fread(magic, sizeof(char), sizeof(magic), pakFile) != sizeof(magic)) {
        return 0;
    }
    return strncmp(magic, "IWAD", 4) == 0 || strncmp(magic, "PWAD", 4) == 0;
}

static int save(TreeNode* treeRoot, const char* where)
{
    FILE* file = fopen(where, "w");
    if (!file) {
        return -1;
    }
    fclose(file);
    return 0;
}

static TreeNode* build(void* pakBuf, size_t pakBufSize, const struct stat* pakStat)
{
    if (pakBufSize < sizeof(WadHeader)) {
        fprintf(stderr, "Invalid or corrupted file\n");
        return NULL;
    }
    
    WadHeader* header = (WadHeader*)pakBuf;
    
    if (header->diroffset > pakBufSize || header->dirsize * sizeof(WadEntry) > pakBufSize || 
        header->diroffset + header->dirsize * sizeof(WadEntry) > pakBufSize
    ) {
        fprintf(stderr, "Invalid dir data\n");
        return NULL;
    }
    
    TreeNode* treeRoot = createNode("");
    treeRoot->atime = pakStat->st_atime;
    treeRoot->mtime = pakStat->st_mtime;
    
    void* cur = pakBuf + header->diroffset;
    size_t i;
    for (i=0; i<header->dirsize; ++i) {
        WadEntry* entry = (WadEntry*) cur;
        if (entry->filepos > pakBufSize || entry->filelen > pakBufSize || 
            entry->filepos + entry->filelen > pakBufSize) {
            fprintf(stderr, "Invalid wad entry\n");
            return NULL;
        }
        
        char buf[9];
        strncpy(buf, entry->name, 8);
        buf[8] = '\0';
        TreeNode* child = createNode(buf);
        child->atime = pakStat->st_atime;
        child->mtime = pakStat->st_mtime;
        void* data = malloc(entry->filelen);
        memcpy(data, pakBuf+entry->filepos, entry->filelen);
        child->data = data;
        child->size = entry->filelen;
        child->type = S_IFREG;
        
        addChildNode(treeRoot, child);
        
        cur += sizeof(WadEntry);
    }
    
    return treeRoot;
}

void initWadFormat(struct pakformat *handler)
{
    strcpy(handler->name, "wad");
    handler->canRead = canRead;
    handler->build = build;
    handler->save = NULL;
    handler->maxFileNameLength = 9;
    handler->supportsSubdirs = 0;
}
