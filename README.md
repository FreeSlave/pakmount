# Pakmount

**pakmount** is a command line tool making usage of [FUSE](https://en.wikipedia.org/wiki/Filesystem_in_Userspace) to mount archives of specific formats used in several games as virtual file systems. The goal is to provide transparent access to archive contents without requiring an additional tool or file manager plugin. The archive mounted in that way is accessible just like any other directory in user space. It that sense pakmount is very similiar to [archivemount](https://en.wikipedia.org/wiki/Archivemount). 

## Supported archive formats

1. PAK is used by several engines and games, e.g. Quake, Quake II and Half-Life.
2. WAD is used in Doom 1, Doom 2 and some other old games. Note that currently only original WAD format is supported, i.e. texture packs used in Half-Life will not work!

## Supported platforms

FUSE is the feature of Unix-like operating systems, therefore pakmount is not available on non-Unix platforms. 
Pakmount was tested on the following systems:

1. GNU/Linux Debian 8 (x86), libfuse 2.9.3
2. GNU/Linux Gentoo (x86_64), libfuse 2.9.3
3. OS X Yosemite 10.10.4, osxfuse 2.8

## Installing dependencies

### Debian

    sudo apt-get install libfuse-dev

### OSX

Install [OSXFUSE](https://osxfuse.github.io/).

## Building from source

To build pakmount type in terminal:

    make

File named **pakmount** should be placed in *bin* directory.

## Usage

### Mounting

Mounting pak file to mountpoint:

    ./bin/pakmount pakfile mountpoint

Mounting in debug mode:

    ./bin/pakmount -d pakfile mountpoint

By default the archive file is mounted as readonly. Mount as writable:

    ./bin/pakmount -o writable pakfile mountpoint

Changes are written upon the unmounting.
Note: when mounting archive as writable file system the backup is created automatically in the same directory as the original archive. If you don't need this functionality use **nobackup** option:

    ./bin/pakmount -o nobackup -o writable pakfile mountpoint

Another option is **nosave**. It leaves original archive unchanged even if its contents were modified during the mount.

    ./bin/pakmount -o nosave -o writable pakfile mountpoint

### Unmounting

    fusermount -uz mountpoint
    
Or when in debug mode just interrupt pakmount at terminal (Ctrl+C).

On OSX:

    umount mountpoint

## Current limitations

1. No hardlinks nor symlinks are supported (Why would you need them inside an archive?).
2. chown and chmod are available, but do nothing. The file permissions are constant and depends on how you mount the archive.
3. No support for big endian machines, since I would not be able to check this anyway. If you have one and want to use pakmount then let me know!

## License

See LICENSE.txt for licensing information.

## Alternatives

1. [fusepak](http://fusepak.sourceforge.net/) - supports various archive formats including PAK, WAD and WAD2. Does not support writing. The compiled binary file is called **pakmount** too, so don't confuse it with this project! (Probably I need to change the name of mine. That's what you get when making the utility without preliminary research - you end up with the same name!).
2. [GridMount](https://codingrange.com/gridmount) - supports GCF and VPK files used in Source engine games. Available only on OSX.
